# Hyojun.couch-bootstrap Snippets para o Sublime Text

Este repositório contém todos os snippets a serem instalados via [Package Control](https://packagecontrol.io/). Apenas faz sentido para usuários do projeto [Hyojun.couch-bootstrap](https://bitbucket.org/fbiz/hyojun.couch-bootstrap).

A lista de código fonte abaixo mostra o ponto do tab com os `$1`, `$2`, etc. o valor padrão quando houver `${1:valor}` e `$0` é o último tab:

## kedit ↦

Existem 3 opções de `kedit`:

### Image

Escreve a tag editable para imagens, considerando o show preview como opcional:

```
<cms:editable name="$1"
    label="$2"
    type="image"
    ${3:show_preview="1"
    preview_width="${4:200}"
    preview_height="${5:200}"}
>${6:$SELECTION}</cms:editable>$0
```

### Richtext

Escreve a tag editable para richtext, já considerando os valores padrões do couch-bootstrap:

```
<cms:editable name="$1"
    label="$2"
    type="richtext"
    toolbar="custom"
    custom_toolbar="${3:<cms:show rt_default />}"
    height="${4:<cms:show rt_height />}"
>${5:$SELECTION}</cms:editable>$0
```

### Vazio

Escreve a tag editable para ser preenchida, útil para os tipos restantes de editable:

```
<cms:editable name="$1"
    label="$2"
    type="${3:text}"
>${4:$SELECTION}</cms:editable>$0
```

## kshow ↦

Escreve a tag show:

```
<cms:show ${1:$SELECTION} />$0
```

## kset ↦

Escreve a tag set:

```
<cms:set ${1:$SELECTION}="$2" />$0
```

## kembed ↦

Escreve a tag embed:

```
<cms:embed "${1:$SELECTION}" />$0
```

## khelper ↦

Escreve a tag set e a tag embed em seguida. O valor da tag set é preenchido automaticamente com o caminho do helper.

```
${2:<cms:set hlp_${1/\//_/g}_$3="$4" />}
<cms:embed "helpers/${1:$SELECTION}.php" />$0
```

## kif ↦

Escreve a tag if:

```
<cms:if ${1:$SELECTION} ${2:==} $3>$4</cms:if>$0
```

## kthumb ↦

Escreve a tag thumbnail

```
<cms:thumbnail ${1:$SELECTION} width='$2' ${3:height='$4' crop='${5:1}'} />$0
```

## ktag ↦

Snippet abstrato para criação de qualquer tag:

```
<cms:${1:$SELECTION} $2 />$0
```
